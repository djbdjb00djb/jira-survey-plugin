package com.atlassian.jira.plugins.survey;

public interface PollOption
{
    Long getOptionId();

    String getValue();

    Long getSequence();
}
