package com.atlassian.jira.plugins.survey;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;

import java.util.Collection;

public abstract class DataHandler
{
    protected final Issue issue;
    protected final JiraAuthenticationContext jiraAuthenticationContext;

    protected DataHandler(Issue issue, JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.issue = issue;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public abstract String getPollId();

    public abstract void validate() throws ValidationException;

    public abstract Collection<PollOption> getOptions();

    public abstract Collection<OptionWithVoters> getCurrentValue();

    public abstract void update(Collection<OptionWithVoters> oldValues,
                Collection<OptionWithVoters> newValues);

    public abstract String getHtml();
}
