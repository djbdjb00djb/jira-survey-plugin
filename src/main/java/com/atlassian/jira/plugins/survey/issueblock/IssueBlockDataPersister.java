package com.atlassian.jira.plugins.survey.issueblock;

import com.atlassian.jira.plugins.survey.PollOption;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class IssueBlockDataPersister
{
    public static final String POLL_ID_PREFIX = "IB";

    private static final String ISSUE_POLLS_KEY_PREFIX = "jira.survey.plugin.issue.polls:";
    private static final String POLL_KEY_PREFIX = "jira.survey.plugin.poll:";
    private static final String VALUES_KEY_PREFIX = "jira.survey.plugin.values:";
    private static final String OPTION_KEY_PREFIX = "jira.survey.plugin.option:";

    private static final String POLL_ID = "pollId";
    private static final String NAME = "name";
    private static final String OPTION_ID = "optionId";
    private static final String VALUE = "value";
    private static final String SEQUENCE = "sequence";

    private final PluginSettingsFactory pluginSettingsFactory;

    public IssueBlockDataPersister(PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @SuppressWarnings("unchecked")
    public List<Poll> getIssuePolls(String issueKey)
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        List<String> pollIds = (List<String>) settings.get(ISSUE_POLLS_KEY_PREFIX + issueKey);

        if (pollIds == null)
        {
            return Collections.emptyList();
        }

        List<Poll> polls = Lists.newArrayListWithCapacity(pollIds.size());

        for (String pollId : pollIds)
        {
            Poll poll = getPoll(pollId);
            polls.add(poll);
        }

        Collections.sort(polls, new PollComparator());

        return polls;
    }

    private void saveIssuePolls(String issueKey, List<Poll> polls)
    {
        List<String> pollIds = Lists.newArrayListWithCapacity(polls.size());

        for (Poll poll : polls)
        {
            pollIds.add(poll.getPollId());
        }

        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
        settings.put(ISSUE_POLLS_KEY_PREFIX + issueKey, pollIds);
    }

    public Poll getPoll(String pollId)
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        @SuppressWarnings("unchecked")
        Map<String, String> pollMap = (Map<String, String>) settings.get(POLL_KEY_PREFIX + pollId);

        if (pollMap == null)
        {
            return null;
        }

        Long sequence = Long.valueOf(pollMap.get(SEQUENCE));
        String name = pollMap.get(NAME);

        return new Poll(pollId, name, sequence);
    }

    public void savePoll(Poll poll)
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        Map<String, String> pollMap = Maps.newHashMap();
        pollMap.put(POLL_ID, poll.getPollId());
        pollMap.put(NAME, poll.getName());
        pollMap.put(SEQUENCE, String.valueOf(poll.getSequence()));

        settings.put(POLL_KEY_PREFIX + poll.getPollId(), pollMap);
    }

    public boolean getExists(String pollId)
    {
        return getPollTextValues(pollId) != null;
    }

    public PollOption getOption(long optionId)
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        @SuppressWarnings("unchecked")
        Map<String, String> option = (Map<String, String>) settings.get(OPTION_KEY_PREFIX + optionId);

        if (option == null)
        {
            return null;
        }

        String value = option.get(VALUE);
        Long sequence = Long.valueOf(option.get(SEQUENCE));

        return new IssueBlockPollOption(optionId, value, sequence);
    }

    @SuppressWarnings("unchecked")
    public List<String> getPollTextValues(String pollId)
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        return (List<String>) settings.get(VALUES_KEY_PREFIX + pollId);
    }

    public void update(String pollId, List<String> newValues)
    {
        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        settings.put(VALUES_KEY_PREFIX + pollId, newValues);
    }

    public void add(String issueKey, String name)
    {
        List<Poll> issuePolls = new ArrayList<Poll>(getIssuePolls(issueKey));

        Poll newPoll = new Poll(POLL_ID_PREFIX + RandomStringUtils.randomAlphanumeric(5),
                name, (long) issuePolls.size());
        savePoll(newPoll);

        issuePolls.add(newPoll);

        saveIssuePolls(issueKey, issuePolls);
    }
}
