package com.atlassian.jira.plugins.survey.issueblock;

import com.atlassian.jira.plugins.survey.OptionParser;
import com.atlassian.jira.plugins.survey.PollOption;

public class IssueBlockOptionParser extends OptionParser
{
    private final IssueBlockDataPersister issueBlockDataPersister;

    public IssueBlockOptionParser(IssueBlockDataPersister issueBlockDataPersister)
    {
        this.issueBlockDataPersister = issueBlockDataPersister;
    }

    @Override
    protected PollOption getPollOption(Long optionId)
    {
        return issueBlockDataPersister.getOption(optionId);
    }
}
