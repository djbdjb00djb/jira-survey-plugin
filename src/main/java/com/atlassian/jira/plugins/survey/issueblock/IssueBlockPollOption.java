package com.atlassian.jira.plugins.survey.issueblock;

import com.atlassian.jira.plugins.survey.PollOption;

public class IssueBlockPollOption implements PollOption
{
    private final Long optionId;
    private final String value;
    private final Long sequence;

    public IssueBlockPollOption(Long optionId, String value, Long sequence)
    {
        this.optionId = optionId;
        this.value = value;
        this.sequence = sequence;
    }

    @Override
    public Long getOptionId()
    {
        return optionId;
    }

    @Override
    public String getValue()
    {
        return value;
    }

    @Override
    public Long getSequence()
    {
        return sequence;
    }
}
