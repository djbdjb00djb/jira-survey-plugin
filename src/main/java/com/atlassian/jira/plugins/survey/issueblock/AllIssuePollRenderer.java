package com.atlassian.jira.plugins.survey.issueblock;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

public class AllIssuePollRenderer
{
    private final TemplateRenderer renderer;
    private final PollContextProvider pollContextProvider;
    private final IssueManager issueManager;

    public AllIssuePollRenderer(TemplateRenderer renderer, PollContextProvider pollContextProvider, IssueManager issueManager)
    {
        this.renderer = renderer;
        this.pollContextProvider = pollContextProvider;
        this.issueManager = issueManager;
    }

    public String getHtml(String issueKey)
    {
        Issue issue = issueManager.getIssueObject(issueKey);

        @SuppressWarnings("unchecked")
        Map<String, Object> params = pollContextProvider.getContextMap(issue);

        StringWriter writer = new StringWriter();
        try
        {
            renderer.render("templates/issue-block-view.vm", params, writer);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        return writer.toString();
    }
}
