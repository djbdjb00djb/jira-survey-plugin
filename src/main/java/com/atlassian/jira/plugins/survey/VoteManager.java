package com.atlassian.jira.plugins.survey;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.util.Collection;
import java.util.Set;

public abstract class VoteManager
{
    private final IssueManager issueManager;
    protected final JiraAuthenticationContext jiraAuthenticationContext;
    private final PermissionManager permissionManager;
    protected final OptionParser optionParser;

    public VoteManager(IssueManager issueManager, JiraAuthenticationContext jiraAuthenticationContext,
                       PermissionManager permissionManager, OptionParser optionParser)
    {
        this.issueManager = issueManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.permissionManager = permissionManager;
        this.optionParser = optionParser;
    }

    protected abstract DataHandler getDataHandler(MutableIssue issue, String pollId);

    public void update(String issueKey, String pollId, Long optionId) throws ValidationException
    {
        MutableIssue issue = getIssue(issueKey);
        DataHandler dataHandler = getDataHandler(issue, pollId);

        validate(issue, dataHandler, optionId);

        String voter = getVoter(issue);

        Collection<OptionWithVoters> oldValues = dataHandler.getCurrentValue();
        Collection<OptionWithVoters> newValues = Lists.newArrayListWithCapacity(oldValues.size());

        boolean added = false;
        for (OptionWithVoters optionWithVoters : oldValues)
        {
            Set<String> voters = Sets.newHashSet();
            voters.addAll(optionWithVoters.getVoters());

            if (optionWithVoters.getOptionId().equals(optionId))
            {
                voters.add(voter);
                added = true;
            }
            else
            {
                voters.remove(voter);
            }

            newValues.add(new OptionWithVoters(optionWithVoters.getOption(), voters));
        }

        // Looks like this option was added to the configuration later, that is why it's not present in the old value. Lets add it.
        if (optionId != null && !added)
        {
            PollOption option = optionParser.getPollOption(optionId);
            if (option != null)
            {
                Set<String> voters = Sets.newHashSet();
                voters.add(voter);
                newValues.add(new OptionWithVoters(option, voters));
            }
        }

        dataHandler.update(oldValues, newValues);
    }

    private void validate(MutableIssue issue, DataHandler dataHandler, Long optionId) throws ValidationException
    {
        dataHandler.validate();

        if (optionId == null)
        {
            return;
        }

        Collection<PollOption> options = dataHandler.getOptions();

        if (options != null)
        {
            for (PollOption option : options)
            {
                if (option.getOptionId().equals(optionId))
                {
                    return;
                }
            }
        }

        throw validationException("survey.invalid.option");
    }

    private MutableIssue getIssue(String issueKey) throws ValidationException
    {
        MutableIssue issue = issueManager.getIssueObject(issueKey);

        if (issue == null)
        {
            throw validationException("survey.no.issue");
        }

        return issue;
    }

    private String getVoter(MutableIssue issue) throws ValidationException
    {
        User loggedInUser = jiraAuthenticationContext.getLoggedInUser();

        if (loggedInUser == null)
        {
            throw validationException("survey.not.logged.in");
        }

        if (!permissionManager.hasPermission(Permissions.BROWSE, issue, loggedInUser))
        {
            throw validationException("survey.no.permission");
        }

        return loggedInUser.getName();
    }

    private ValidationException validationException(String key)
    {
        return new ValidationException(getText(key));
    }

    private String getText(String key)
    {
        return jiraAuthenticationContext.getI18nHelper().getText(key);
    }

    public String getHtml(String issueKey, String pollId) throws ValidationException
    {
        MutableIssue issue = getIssue(issueKey);
        DataHandler dataHandler = getDataHandler(issue, pollId);

        return dataHandler.getHtml();
    }
}
