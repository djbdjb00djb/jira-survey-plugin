package com.atlassian.jira.plugins.survey;

import com.atlassian.jira.plugins.survey.issueblock.AllIssuePollRenderer;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/jira-poll")
public class PollResource
{
    private final VoteManagerProvider voteManagerProvider;
    private final AllIssuePollRenderer allIssuePollRenderer;

    public PollResource(VoteManagerProvider voteManagerProvider, AllIssuePollRenderer allIssuePollRenderer)
    {
        this.voteManagerProvider = voteManagerProvider;
        this.allIssuePollRenderer = allIssuePollRenderer;
    }

    @Path("/{issueKey}/{pollId}/{optionId}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response vote(@PathParam("issueKey") String issueKey, @PathParam("pollId") String pollId, @PathParam ("optionId") Long optionId)
    {
        try
        {
            return update(issueKey, pollId, optionId);
        }
        catch (ValidationException e)
        {
            return createErrorResponse(e);
        }
    }

    @Path("/{issueKey}/{pollId}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteVote(@PathParam("issueKey") String issueKey, @PathParam("pollId") String pollId)
    {
        try
        {
            return update(issueKey, pollId, null);
        }
        catch (ValidationException e)
        {
            return createErrorResponse(e);
        }
    }

    @Path("/{issueKey}/{pollId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getContent(@PathParam("issueKey") String issueKey, @PathParam("pollId") String pollId)
    {
        try
        {
            return createResponse(getVoteManager(pollId).getHtml(issueKey, pollId));
        }
        catch (ValidationException e)
        {
            return createErrorResponse(e);
        }
    }

    @Path("/new-poll/{issueKey}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response newPoll(@PathParam("issueKey") String issueKey, @QueryParam("name") String name)
    {
        try
        {
            voteManagerProvider.getIssueBlockVoteManager().addPoll(issueKey, name);

            return createResponse(allIssuePollRenderer.getHtml(issueKey));
        }
        catch (ValidationException e)
        {
            return createErrorResponse(e);
        }
    }

    @Path("/edit-poll/{issueKey}/{pollId}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response editPoll(@PathParam("issueKey") String issueKey, @PathParam("pollId") String pollId, @QueryParam("name") String name)
    {
        try
        {
            voteManagerProvider.getIssueBlockVoteManager().editPoll(pollId, name);

            return createResponse(allIssuePollRenderer.getHtml(issueKey));
        }
        catch (ValidationException e)
        {
            return createErrorResponse(e);
        }
    }

    @Path("/issue-poll/{pollId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIssuePoll(@PathParam("pollId") String pollId)
    {
        return Response.ok(voteManagerProvider.getIssueBlockVoteManager().getPoll(pollId)).build();
    }

    private Response update(String issueKey, String pollId, Long optionId) throws ValidationException
    {
        getVoteManager(pollId).update(issueKey, pollId, optionId);

        return createResponse(getVoteManager(pollId).getHtml(issueKey, pollId));
    }

    private VoteManager getVoteManager(String pollId) throws ValidationException
    {
        return voteManagerProvider.getVoteManager(pollId);
    }

    private Response createErrorResponse(ValidationException e)
    {
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(e.getMessage())
                .build();
    }

    private Response createResponse(String content)
    {
        return Response.ok(new ResponseContainer(content)).build();
    }

    public static class ResponseContainer
    {
        @JsonProperty
        private final String html;

        public ResponseContainer(String html)
        {
            this.html = html;
        }

        public String getHtml()
        {
            return html;
        }
    }
}
