package com.atlassian.jira.plugins.survey;

import com.atlassian.jira.plugins.survey.customfield.CustomFieldVoteManager;
import com.atlassian.jira.plugins.survey.issueblock.IssueBlockDataPersister;
import com.atlassian.jira.plugins.survey.issueblock.IssueBlockVoteManager;

public class VoteManagerProvider
{
    private final CustomFieldVoteManager customFieldVoteManager;
    private final IssueBlockVoteManager issueBlockVoteManager;

    public VoteManagerProvider(CustomFieldVoteManager customFieldVoteManager, IssueBlockVoteManager issueBlockVoteManager)
    {
        this.customFieldVoteManager = customFieldVoteManager;
        this.issueBlockVoteManager = issueBlockVoteManager;
    }

    public VoteManager getVoteManager(String pollId) throws ValidationException
    {
        if (pollId.startsWith(IssueBlockDataPersister.POLL_ID_PREFIX))
        {
            return issueBlockVoteManager;
        }

        return customFieldVoteManager;
    }

    public IssueBlockVoteManager getIssueBlockVoteManager()
    {
        return issueBlockVoteManager;
    }
}
