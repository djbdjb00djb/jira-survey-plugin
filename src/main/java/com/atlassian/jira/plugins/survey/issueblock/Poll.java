package com.atlassian.jira.plugins.survey.issueblock;

import org.codehaus.jackson.annotate.JsonProperty;

public class Poll
{
    @JsonProperty
    private String pollId;
    @JsonProperty
    private String name;
    @JsonProperty
    private Long sequence;

    public Poll(String pollId, String name, Long sequence)
    {
        this.pollId = pollId;
        this.name = name;
        this.sequence = sequence;
    }

    public String getPollId()
    {
        return pollId;
    }

    public void setPollId(String pollId)
    {
        this.pollId = pollId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Long getSequence()
    {
        return sequence;
    }

    public void setSequence(Long sequence)
    {
        this.sequence = sequence;
    }
}
