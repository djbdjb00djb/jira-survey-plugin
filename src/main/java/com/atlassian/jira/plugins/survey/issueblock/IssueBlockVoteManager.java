package com.atlassian.jira.plugins.survey.issueblock;

import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.plugins.survey.DataHandler;
import com.atlassian.jira.plugins.survey.ValidationException;
import com.atlassian.jira.plugins.survey.VoteManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import org.apache.commons.lang.StringUtils;

public class IssueBlockVoteManager extends VoteManager
{
    private final IssueBlockDataPersister issueBlockDataPersister;

    public IssueBlockVoteManager(IssueManager issueManager, JiraAuthenticationContext jiraAuthenticationContext,
                                 PermissionManager permissionManager, IssueBlockDataPersister issueBlockDataPersister)
    {
        super(issueManager, jiraAuthenticationContext, permissionManager, new IssueBlockOptionParser(issueBlockDataPersister));
        this.issueBlockDataPersister = issueBlockDataPersister;
    }

    @Override
    protected DataHandler getDataHandler(MutableIssue issue, String pollId)
    {
        return new IssueBlockDataHandler(issue, pollId, issueBlockDataPersister, jiraAuthenticationContext, optionParser);
    }

    public void addPoll(String issueKey, String name) throws ValidationException
    {
        if (StringUtils.isBlank(name))
        {
            throw new ValidationException(jiraAuthenticationContext.getI18nHelper().getText("survey.blank.name"));
        }

        issueBlockDataPersister.add(issueKey, name);
    }

    public Poll getPoll(String pollId)
    {
        return issueBlockDataPersister.getPoll(pollId);
    }

    public void editPoll(String pollId, String name) throws ValidationException
    {
        Poll poll = getPoll(pollId);
        if (poll == null)
        {
            throw new ValidationException(jiraAuthenticationContext.getI18nHelper().getText("survey.no.poll"));
        }

        poll.setName(name);

        issueBlockDataPersister.savePoll(poll);
    }
}
