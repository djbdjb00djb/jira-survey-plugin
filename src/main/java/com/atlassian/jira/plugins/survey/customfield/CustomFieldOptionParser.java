package com.atlassian.jira.plugins.survey.customfield;

import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.plugins.survey.OptionParser;
import com.atlassian.jira.plugins.survey.PollOption;

public class CustomFieldOptionParser extends OptionParser
{
    private final OptionsManager optionsManager;

    public CustomFieldOptionParser(OptionsManager optionsManager)
    {
        this.optionsManager = optionsManager;
    }

    @Override
    protected PollOption getPollOption(Long optionId)
    {
        Option option = optionsManager.findByOptionId(optionId);
        if (option == null)
        {
            return null;
        }

        return new CustomFieldPollOption(option);
    }
}
