package com.atlassian.jira.plugins.survey;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class VotesHelper
{
    public long getPercents(long value, long total)
    {
        if (value == 0)
        {
            return 0;
        }

        if (total == 0)
        {
            return 0;
        }

        if (value == total)
        {
            return 100;
        }

        BigDecimal result = BigDecimal.valueOf(value)
                .multiply(BigDecimal.valueOf(100))
                .divide(BigDecimal.valueOf(total), 0, RoundingMode.HALF_UP);

        return result.longValue();
    }
}
