package com.atlassian.jira.plugins.survey;

import java.util.Set;

public class OptionWithVoters
{
    private final PollOption option;
    private final Set<String> voters;

    public OptionWithVoters(PollOption option, Set<String> voters)
    {
        this.option = option;
        this.voters = voters;
    }

    public PollOption getOption()
    {
        return option;
    }

    public Set<String> getVoters()
    {
        return voters;
    }

    public Long getOptionId()
    {
        return option.getOptionId();
    }
}
