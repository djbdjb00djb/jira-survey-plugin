package com.atlassian.jira.plugins.survey.issueblock;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.plugins.survey.OptionParser;
import com.atlassian.jira.plugins.survey.VelocityParameterProvider;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class PollContextProvider extends AbstractJiraContextProvider
{
    private final VelocityParameterProvider velocityParameterProvider;
    private final IssueBlockDataPersister issueBlockDataPersister;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final OptionParser optionParser;
    private final TemplateRenderer renderer;

    public PollContextProvider(VelocityParameterProvider velocityParameterProvider,
                               IssueBlockDataPersister issueBlockDataPersister,
                               JiraAuthenticationContext jiraAuthenticationContext,
                               TemplateRenderer renderer)
    {
        this.velocityParameterProvider = velocityParameterProvider;
        this.issueBlockDataPersister = issueBlockDataPersister;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.renderer = renderer;
        this.optionParser = new IssueBlockOptionParser(issueBlockDataPersister);
    }

    @Override
    public Map getContextMap(User user, JiraHelper jiraHelper)
    {
        Issue issue = (Issue) jiraHelper.getContextParams().get("issue");

        return getContextMap(issue);
    }

    public Map getContextMap(Issue issue)
    {
        List<Poll> issuePolls = issueBlockDataPersister.getIssuePolls(issue.getKey());

        if (CollectionUtils.isEmpty(issuePolls))
        {
            return Collections.emptyMap();
        }

        Map<Poll, String> pollHtmls = Maps.newLinkedHashMap();

        for (Poll poll : issuePolls)
        {
            IssueBlockDataHandler dataHandler = new IssueBlockDataHandler(issue, poll.getPollId(),
                    issueBlockDataPersister, jiraAuthenticationContext, optionParser);
            Map<String, Object> pollParams = velocityParameterProvider.getParams(issue, dataHandler);

            StringWriter writer = new StringWriter();
            try
            {
                renderer.render("templates/poll-field-view.vm", pollParams, writer);
            } catch (IOException e)
            {
                throw new RuntimeException(e);
            }

            String html = writer.toString();
            pollHtmls.put(poll, html);
        }

        Map<String, Object> params = Maps.newHashMap();

        params.put("pollHtmls", pollHtmls);

        return params;
    }
}
