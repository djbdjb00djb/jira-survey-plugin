package com.atlassian.jira.plugins.survey;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.Maps;

import java.util.Collection;
import java.util.Map;

public class VelocityParameterProvider
{
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public VelocityParameterProvider(JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public Map<String, Object> getParams(Issue issue, DataHandler dataHandler)
    {
        Map<String, Object> params = Maps.newHashMap();

        User loggedInUser = jiraAuthenticationContext.getLoggedInUser();
        params.put("currentUser", loggedInUser);

        params.put("pollId", dataHandler.getPollId());

        if (issue == null)
        {
            return params;
        }

        params.put("options", dataHandler.getOptions());

        if (loggedInUser != null)
        {
            @SuppressWarnings("unchecked")
            Collection<OptionWithVoters> currentValue = dataHandler.getCurrentValue();
            if (currentValue != null)
            {
                for (OptionWithVoters optionWithVoters : currentValue)
                {
                    if (optionWithVoters.getVoters().contains(loggedInUser.getName()))
                    {
                        params.put("currentVote", optionWithVoters.getOptionId());
                        break;
                    }
                }
            }

            params.put("reporterOrAssignee", loggedInUser.equals(issue.getReporter()) || loggedInUser.equals(issue.getAssignee()));
        }

        params.put("votesHelper", new VotesHelper());

        return params;
    }
}
