package com.atlassian.jira.plugins.survey.issueblock;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugins.survey.*;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class IssueBlockDataHandler extends DataHandler
{
    private final String pollId;
    private final IssueBlockDataPersister issueBlockDataPersister;
    private final OptionParser optionParser;

    protected IssueBlockDataHandler(Issue issue, String pollId,
                                    IssueBlockDataPersister issueBlockDataPersister,
                                    JiraAuthenticationContext jiraAuthenticationContext,
                                    OptionParser optionParser)
    {
        super(issue, jiraAuthenticationContext);
        this.pollId = pollId;
        this.issueBlockDataPersister = issueBlockDataPersister;
        this.optionParser = optionParser;
    }

    @Override
    public String getPollId()
    {
        return pollId;
    }

    @Override
    public void validate() throws ValidationException
    {
        if (!issueBlockDataPersister.getExists(pollId))
        {
            throw new ValidationException(jiraAuthenticationContext.getI18nHelper().getText("survey.no.poll"));
        }
    }

    @Override
    public Collection<PollOption> getOptions()
    {
        List<String> textValues = issueBlockDataPersister.getPollTextValues(pollId);
        if (textValues == null)
        {
            return Collections.emptyList();
        }

        List<PollOption> options = Lists.newArrayListWithCapacity(textValues.size());

        for (String textValue : textValues)
        {
            options.add(optionParser.getOptionFromStringValue(textValue));
        }

        return options;
    }

    @Override
    public Collection<OptionWithVoters> getCurrentValue()
    {
        List<String> textValues = issueBlockDataPersister.getPollTextValues(pollId);
        if (textValues == null)
        {
            return Collections.emptyList();
        }

        List<OptionWithVoters> optionsWithVoters = Lists.newArrayListWithCapacity(textValues.size());

        for (String textValue : textValues)
        {
            optionsWithVoters.add(optionParser.getOptionWithVotersFromStringValue(textValue));
        }

        return optionsWithVoters;
    }

    @Override
    public void update(Collection<OptionWithVoters> oldValues, Collection<OptionWithVoters> newValues)
    {
        List<String> textValues = Lists.newArrayListWithCapacity(newValues.size());

        for (OptionWithVoters optionWithVoters : newValues)
        {
            textValues.add(optionParser.getStringFromObject(optionWithVoters));
        }

        issueBlockDataPersister.update(pollId, textValues);
    }

    @Override
    public String getHtml()
    {
        return null;
    }
}
