package com.atlassian.jira.plugins.survey;

import com.google.common.collect.Sets;
import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.Set;

public abstract class OptionParser
{
    public static final String SEPARATOR = ";";
    private static final String USERNAME_SEPARATOR = ",";

    public String getStringFromObject(OptionWithVoters optionWithVoters)
    {
        if (optionWithVoters == null)
        {
            return null;
        }
        return optionWithVoters.getOptionId() + SEPARATOR + StringUtils.join(optionWithVoters.getVoters(), USERNAME_SEPARATOR);
    }

    public PollOption getOptionFromStringValue(String stringValue)
    {
        if (stringValue == null || !stringValue.contains(SEPARATOR))
        {
            return null;
        }

        int separatorIndex = stringValue.indexOf(SEPARATOR);
        Long optionId = Long.valueOf(stringValue.substring(0, separatorIndex));

        return getPollOption(optionId);
    }

    public OptionWithVoters getOptionWithVotersFromStringValue(String stringValue)
    {
        PollOption option = getOptionFromStringValue(stringValue);
        if (option == null)
        {
            return null;
        }

        int separatorIndex = stringValue.indexOf(SEPARATOR);

        Set<String> voters = Sets.newHashSet();
        if (stringValue.length() > separatorIndex + 1)
        {
            voters.addAll(Arrays.asList(stringValue.substring(separatorIndex + 1).split(USERNAME_SEPARATOR)));
        }

        return new OptionWithVoters(option, voters);
    }

    protected abstract PollOption getPollOption(Long optionId);
}
