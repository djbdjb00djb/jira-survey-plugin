package com.atlassian.jira.plugins.survey;

import java.util.Comparator;

public class OptionWithVotersComparator implements Comparator<OptionWithVoters>
{
    @Override
    public int compare(OptionWithVoters o1, OptionWithVoters o2)
    {
        return o1.getOption().getSequence().compareTo(o2.getOption().getSequence());
    }
}
