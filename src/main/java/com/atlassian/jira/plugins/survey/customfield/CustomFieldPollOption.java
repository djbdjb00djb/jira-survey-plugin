package com.atlassian.jira.plugins.survey.customfield;

import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.plugins.survey.PollOption;

public class CustomFieldPollOption implements PollOption
{
    private final Option option;

    public CustomFieldPollOption(Option option)
    {
        this.option = option;
    }

    @Override
    public Long getOptionId()
    {
        return option.getOptionId();
    }

    @Override
    public String getValue()
    {
        return option.getValue();
    }

    @Override
    public Long getSequence()
    {
        return option.getSequence();
    }
}
