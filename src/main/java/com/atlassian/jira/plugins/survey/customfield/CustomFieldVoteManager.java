package com.atlassian.jira.plugins.survey.customfield;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.plugins.survey.DataHandler;
import com.atlassian.jira.plugins.survey.VoteManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;

public class CustomFieldVoteManager extends VoteManager
{
    private final OptionsManager optionsManager;
    private final CustomFieldManager customFieldManager;

    public CustomFieldVoteManager(IssueManager issueManager, JiraAuthenticationContext jiraAuthenticationContext,
                                  PermissionManager permissionManager, OptionsManager optionsManager, CustomFieldManager customFieldManager)
    {
        super(issueManager, jiraAuthenticationContext, permissionManager, new CustomFieldOptionParser(optionsManager));
        this.optionsManager = optionsManager;
        this.customFieldManager = customFieldManager;
    }

    @Override
    protected DataHandler getDataHandler(MutableIssue issue, String pollId)
    {
        CustomField customField = customFieldManager.getCustomFieldObject(Long.valueOf(pollId));

        return new CustomFieldDataHandler(issue, customField, optionsManager, jiraAuthenticationContext);
    }
}
