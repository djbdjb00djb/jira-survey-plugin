package com.atlassian.jira.plugins.survey.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder;
import com.atlassian.jira.plugins.survey.DataHandler;
import com.atlassian.jira.plugins.survey.OptionWithVoters;
import com.atlassian.jira.plugins.survey.PollOption;
import com.atlassian.jira.plugins.survey.ValidationException;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.Lists;

import java.util.Collection;
import java.util.Collections;

public class CustomFieldDataHandler extends DataHandler
{
    private final CustomField customField;
    private final OptionsManager optionsManager;

    public CustomFieldDataHandler(Issue issue, CustomField customField, OptionsManager optionsManager, JiraAuthenticationContext jiraAuthenticationContext)
    {
        super(issue, jiraAuthenticationContext);
        this.customField = customField;
        this.optionsManager = optionsManager;
    }

    @Override
    public String getPollId()
    {
        return String.valueOf(customField.getIdAsLong());
    }

    @Override
    public void validate() throws ValidationException
    {
        if (customField == null || !(customField.getCustomFieldType() instanceof PollCFType))
        {
            throw new ValidationException(jiraAuthenticationContext.getI18nHelper().getText("survey.no.poll"));
        }

        if (customField.getRelevantConfig(issue) == null)
        {
            throw new ValidationException(jiraAuthenticationContext.getI18nHelper().getText("survey.invalid.poll"));
        }
    }

    @Override
    public Collection<PollOption> getOptions()
    {
        FieldConfig fieldConfig = customField.getRelevantConfig(issue);
        Options options = optionsManager.getOptions(fieldConfig);

        if (options != null)
        {
            Collection<PollOption> pollOptions = Lists.newArrayList();
            for (Option option : options)
            {
                if (option.getDisabled() == null || !option.getDisabled())
                {
                    pollOptions.add(new CustomFieldPollOption(option));
                }
            }

            return pollOptions;
        }

        return Collections.emptyList();
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<OptionWithVoters> getCurrentValue()
    {
        return (Collection<OptionWithVoters>) issue.getCustomFieldValue(customField);
    }

    @Override
    public void update(Collection<OptionWithVoters> oldValues, Collection<OptionWithVoters> newValues)
    {
        customField.updateValue(null, issue, new ModifiedValue(oldValues, newValues), new DefaultIssueChangeHolder());
    }

    @Override
    public String getHtml()
    {
        return customField.getViewHtml(null, null, issue);
    }
}
