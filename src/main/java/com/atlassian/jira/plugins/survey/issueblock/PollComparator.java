package com.atlassian.jira.plugins.survey.issueblock;

import java.util.Comparator;

public class PollComparator implements Comparator<Poll>
{
    @Override
    public int compare(Poll poll1, Poll poll2)
    {
        return poll1.getSequence().compareTo(poll2.getSequence());
    }
}
