package com.atlassian.jira.plugins.survey.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.context.JiraContextNode;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.customfields.MultipleSettableCustomFieldType;
import com.atlassian.jira.issue.customfields.config.item.SettableOptionsConfigItem;
import com.atlassian.jira.issue.customfields.impl.AbstractMultiCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.customfields.persistence.PersistenceFieldType;
import com.atlassian.jira.issue.customfields.view.CustomFieldParams;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.plugins.survey.OptionParser;
import com.atlassian.jira.plugins.survey.OptionWithVoters;
import com.atlassian.jira.plugins.survey.OptionWithVotersComparator;
import com.atlassian.jira.plugins.survey.VelocityParameterProvider;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.ErrorCollection;
import com.google.common.collect.Lists;

import java.util.*;

public class PollCFType extends AbstractMultiCFType<OptionWithVoters> implements MultipleSettableCustomFieldType<Collection<OptionWithVoters>, OptionWithVoters>
{
    private final OptionsManager optionsManager;
    private final SurveyValuePesister surveyValuePesister;
    private final OptionParser optionParser;
    private final VelocityParameterProvider velocityParameterProvider;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public PollCFType(OptionsManager optionsManager, CustomFieldValuePersister valuePersister,
                      GenericConfigManager genericConfigManager, SurveyValuePesister surveyValuePesister,
                      VelocityParameterProvider velocityParameterProvider, JiraAuthenticationContext jiraAuthenticationContext)
    {
        super(valuePersister, genericConfigManager);
        this.optionsManager = optionsManager;
        this.surveyValuePesister = surveyValuePesister;
        this.velocityParameterProvider = velocityParameterProvider;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        optionParser = new CustomFieldOptionParser(optionsManager);
    }

    @Override
    public Collection<OptionWithVoters> getValueFromIssue(CustomField field, Issue issue)
    {
        List<Object> textValues = customFieldValuePersister.getValues(field, issue.getId(), getDatabaseType());

        if (textValues == null || textValues.isEmpty())
        {
            FieldConfig fieldConfig = field.getRelevantConfig(issue);

            Options options = optionsManager.getOptions(fieldConfig);

            List<OptionWithVoters> optionsWithVoters = Lists.newArrayListWithCapacity(options.size());
            for (Option option : options)
            {
                optionsWithVoters.add(new OptionWithVoters(new CustomFieldPollOption(option), Collections.<String>emptySet()));
            }
            return optionsWithVoters;
        }

        return convertDbObjectToTypes(textValues);
    }

    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem)
    {
        Map<String, Object> params = super.getVelocityParameters(issue, field, fieldLayoutItem);

        params.putAll(velocityParameterProvider.getParams(issue, new CustomFieldDataHandler(issue, field, optionsManager, jiraAuthenticationContext)));

        return params;
    }

    @Override
    public Collection<OptionWithVoters> getDefaultValue(FieldConfig fieldConfig)
    {
        return Collections.emptyList();
    }

    @Override
    protected Comparator<OptionWithVoters> getTypeComparator()
    {
        return new OptionWithVotersComparator();
    }

    @Override
    protected Object convertTypeToDbValue(OptionWithVoters value)
    {
        return getStringFromSingularObject(value);
    }

    @Override
    protected OptionWithVoters convertDbValueToType(Object dbValue)
    {
        return getSingularObjectFromString((String) dbValue);
    }

    @Override
    protected PersistenceFieldType getDatabaseType()
    {
        return PersistenceFieldType.TYPE_UNLIMITED_TEXT;
    }

    @Override
    public String getStringFromSingularObject(OptionWithVoters optionWithVoters)
    {
        return optionParser.getStringFromObject(optionWithVoters);
    }

    @Override
    public OptionWithVoters getSingularObjectFromString(String string)
    {
        return getOptionFromStringValue(string);
    }

    private OptionWithVoters getOptionFromStringValue(String stringValue)
    {
        return optionParser.getOptionWithVotersFromStringValue(stringValue);
    }

    @Override
    public void validateFromParams(CustomFieldParams relevantParams, ErrorCollection errorCollectionToAddTo, FieldConfig config)
    {
    }

    @Override
    public Collection<OptionWithVoters> getValueFromCustomFieldParams(CustomFieldParams parameters) throws FieldValidationException
    {
        Collection<?> values = parameters.getAllValues();
        if (CustomFieldUtils.isCollectionNotEmpty(values))
        {
            List<OptionWithVoters> options = new ArrayList<OptionWithVoters>();
            for (Object value : values)
            {
                options.add(getSingularObjectFromString((String) value));
            }
            return options;
        }
        else
        {
            return null;
        }
    }

    @Override
    public Object getStringValueFromCustomFieldParams(CustomFieldParams parameters)
    {
        return parameters.getValuesForNullKey();
    }

    @Override
    public Set<Long> getIssueIdsWithValue(CustomField field, Option option)
    {
        return surveyValuePesister.getIssueIdsWithOption(field, option);
    }

    @Override
    public void removeValue(CustomField field, Issue issue, Option option)
    {
        surveyValuePesister.removeValue(field, issue, option);
    }

    @Override
    public Options getOptions(FieldConfig fieldConfig, JiraContextNode jiraContextNode)
    {
        return optionsManager.getOptions(fieldConfig);
    }

    @Override
    public List<FieldConfigItemType> getConfigurationItemTypes()
    {
        List<FieldConfigItemType> configurationItemTypes = Lists.newArrayList();
        configurationItemTypes.add(new SettableOptionsConfigItem(this, optionsManager));
        return configurationItemTypes;
    }

}
