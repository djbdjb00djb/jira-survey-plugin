package com.atlassian.jira.plugins.survey.customfield;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.CustomFieldUtils;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.plugins.survey.OptionParser;
import org.ofbiz.core.entity.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SurveyValuePesister
{
    private static final String ENTITY_VALUE_TYPE = "valuetype";
    private static final String ENTITY_ISSUE_ID = "issue";
    private static final String ENTITY_CUSTOMFIELD_ID = "customfield";
    private static final String TABLE_CUSTOMFIELD_VALUE = "CustomFieldValue";
    private static final String FIELD_TYPE_TEXT = "textvalue";

    protected final OfBizDelegator delegator;

    public SurveyValuePesister(OfBizDelegator delegator)
    {
        this.delegator = delegator;
    }

    public Set<Long> getIssueIdsWithOption(CustomField field, Option option)
    {
        EntityCondition entityCondition = new EntityConditionList(
                Arrays.asList(
                        new EntityExpr(ENTITY_VALUE_TYPE, EntityOperator.EQUALS, null),
                        new EntityExpr(ENTITY_CUSTOMFIELD_ID, EntityOperator.EQUALS, CustomFieldUtils.getCustomFieldId(field.getId())),
                        new EntityExpr(FIELD_TYPE_TEXT, EntityOperator.LIKE, option.getOptionId() + OptionParser.SEPARATOR + "%")
                ),
                EntityOperator.AND);

        List<GenericValue> values = delegator.findByCondition(TABLE_CUSTOMFIELD_VALUE, entityCondition, null);

        Set<Long> ids = new HashSet<Long>();
        for (GenericValue genericValue : values)
        {
            ids.add(genericValue.getLong(ENTITY_ISSUE_ID));
        }

        return ids;
    }

    public void removeValue(CustomField field, Issue issue, Option option)
    {
        EntityCondition entityCondition = new EntityConditionList(
                Arrays.asList(
                        new EntityExpr(ENTITY_VALUE_TYPE, EntityOperator.EQUALS, null),
                        new EntityExpr(ENTITY_CUSTOMFIELD_ID, EntityOperator.EQUALS, CustomFieldUtils.getCustomFieldId(field.getId())),
                        new EntityExpr(ENTITY_ISSUE_ID, EntityOperator.EQUALS, issue.getId()),
                        new EntityExpr(FIELD_TYPE_TEXT, EntityOperator.LIKE, option.getOptionId() + OptionParser.SEPARATOR + "%")
                ),
                EntityOperator.AND);

        List<GenericValue> values = delegator.findByCondition(TABLE_CUSTOMFIELD_VALUE, entityCondition, null);

        delegator.removeAll(values);
    }

}
