package com.atlassian.jira.plugins.survey;

public class ValidationException extends Exception
{
    public ValidationException(String message)
    {
        super(message);
    }
}
