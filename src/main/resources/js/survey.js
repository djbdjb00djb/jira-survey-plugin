function sendOptionRequest(method, issueKey, pollId, vote)
{
    var url = AJS.contextPath() + "/rest/jira-poll/latest/jira-poll/" + issueKey + "/" + pollId;
    if (vote) {
        url = url + "/" + vote;
    }

    var container = getPollContainer(pollId);
    var loadingIndicator = container.find(".answer-submitting-indicator");
    loadingIndicator.show();

    var submitButton = container.find(".submit-vote");
    var cancelButton = container.find(".cancel-voting");
    submitButton.attr("disabled", "disabled");
    cancelButton.addClass("disabled");

    AJS.$.ajax({
        url: url,
        type: method,
        cache: false,
        timeout: 60000,
        success: function (response) {
            setVotesHtml(pollId, response.html);
        },
        error: function(jqXHR) {
            loadingIndicator.hide();

            submitButton.removeAttr("disabled");
            cancelButton.removeClass("disabled");

            showErrorMessage(jqXHR, container.find(".survey-message-container"));
        }
    });
}

function setVotesHtml(pollId, html) {
    getPollContainer(pollId).replaceWith(jQuery(html));

    var newContainer = getPollContainer(pollId);

    configureVotesTable(newContainer);
}

function getPollContainer(pollId) {
    return jQuery("#poll-container-" + pollId);
}

function configureVotesTable(context) {
    var issueKey = jQuery('meta[name=ajs-issue-key]').attr("content");

    jQuery(context).find(".vote-link").click(function(e) {
        e.preventDefault();

        var pollId = jQuery(this).data("poll-id");

        showVoting(pollId, issueKey);
    });
}

function showVoting(pollId, issueKey) {
    var container = getPollContainer(pollId);

    var options = {};
    container.find(".option").each(function(index, optionResult) {
        optionResult = jQuery(optionResult);
        var optionId = optionResult.data("option-id");
        options[optionId] = jQuery(optionResult.find(".option-name")[0]).text();
    });

    var currentVote = container.data("current-vote");

    var votingContent = JIRA.Templates.Poll.voting({
        options: options,
        pollId: pollId,
        currentVote: currentVote
    });

    container.replaceWith(jQuery(votingContent));

    var newContainer = getPollContainer(pollId);
    newContainer.find(".submit-vote").click(function(e) {
        e.preventDefault();

        var vote = newContainer.find("input:radio[name=vote]:checked").val();

        var method;
        if (vote === "no-vote") {
            method = "delete";
            vote = null;
        } else {
            method = "post";
        }

        sendOptionRequest(method, issueKey, pollId, vote);
    });

    newContainer.find(".cancel-voting").click(function(e) {
        e.preventDefault();

        if (jQuery(e.target).hasClass("disabled"))
        {
            return;
        }

        sendOptionRequest("get", issueKey, pollId, null);
    });
}

function showErrorMessage(jqXHR, messageContainer) {
    var message;
    if ("Unauthorized" === jqXHR.statusText) {
        message = AJS.I18n.getText("survey.not.logged.in");
    } else if (jqXHR.getResponseHeader("Content-Type").indexOf("application/json") < 0) {
        message = AJS.I18n.getText("common.words.error");
    } else {
        message = jqXHR.responseText;
    }

    AJS.messages.error(messageContainer, {
        title: message,
        closeable: true
    });
}

function configureIssuePolls() {
    jQuery(".edit-poll-link").click(function(e) {
        e.preventDefault();

        var pollId = jQuery(e.target).data("poll-id");

        AJS.$.ajax({
            url: AJS.contextPath() + "/rest/jira-poll/latest/jira-poll/issue-poll/" + pollId,
            type: "get",
            cache: false,
            timeout: 60000,
            success: function (poll) {
                showPollDialog(poll);
            }
        });
    });
}

function showPollDialog(poll) {
    var issueKey = jQuery('meta[name=ajs-issue-key]').attr("content");

    var content = jQuery(JIRA.Templates.Poll.editPoll({
        pollName: poll ? poll.name : ""
    }));

    var dialog = new JIRA.FormDialog({
        id: 'add-poll-dialog',
        content: content,
        submitHandler: function(e) {
            e.preventDefault();

            var dialog = this;

            var name = jQuery("#poll-name").val();

            var url = AJS.contextPath() + "/rest/jira-poll/latest/jira-poll/";
            if (poll) {
                url += "edit-poll/" + issueKey + "/" + poll.pollId
            } else {
                url += "new-poll/" + issueKey
            }

            url += "?name=" + encodeURIComponent(name);

            AJS.$.ajax({
                url: url,
                type: "post",
                cache: false,
                timeout: 60000,
                success: function (response) {
                    var container = jQuery("#issue-polls-container");

                    container.replaceWith(jQuery(response.html));

                    configureIssuePolls();

                    dialog.hide();
                },
                error: function(jqXHR) {
                    showErrorMessage(jqXHR, jQuery("#add-poll-message-container"));
                }
            });
        }
    });

    dialog.show();

    var dialogContent = dialog.get$popupContent();

    dialogContent.find(".add-option").click(function(e) {
        e.preventDefault();

        var newOptionNameElement = dialogContent.find("#new-option");
        var optionName = newOptionNameElement.val();
        newOptionNameElement.val("");

        var pollOptionsContainer = dialogContent.find("#poll-options");
        pollOptionsContainer.append("<li>" + optionName + "</li>");
    });
}

jQuery(document).ready(function ()
{
    configureVotesTable(document);
    configureIssuePolls();

    jQuery("#survey-create-poll").click(function(e) {
        e.preventDefault();

        showPollDialog(null);
    });
});